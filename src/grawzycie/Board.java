package grawzycie;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

public class Board extends JPanel implements Runnable {

	private int pixelSize = 10;
	private int width = 600;
	private int height = 520;
	private int tabSizeX = this.width / this.pixelSize;
	private int tabSizeY = this.height / this.pixelSize;
	private P[][] P = new P[this.tabSizeX][this.tabSizeY];

	public Board() {

		for (int i = 0; i < this.tabSizeX; i++) {
			for (int j = 0; j < this.tabSizeY; j++) {
				this.P[i][j] = new P();
			}
		}

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int x = arg0.getX();
				int y = arg0.getY();
				Board.this.clickPixel(x, y);
				Board.this.refresh();
			}
		});

	}

	public void run() {
		while (true) {
			if (Settings.run == true) {
				this.reverse(); 
				this.refresh();
			}
			try {
				Thread.sleep(Settings.delay);
			} catch (Exception ex) {
			}
		}
	}

	public void refresh() {	

		if (Settings.run == true) { // run
			for (int i = 0; i <= this.tabSizeX - 1; i++) {
				for (int j = 0; j <= this.tabSizeY - 1; j++) {
					this.P[i][j].act = this.makeAct(i, j);
				}
			}
		} // run

		repaint();
	}

	private int makeAct(int x, int y) {
		int[] s = { 0, 0, 0, 0, 0, 0, 0, 0 };
		/*
		 * 0 - lewa g�rna; 1 - centralna g�rna; 2 - prawa g�rna; 3 - prawa
		 * �rodkowa 4 - prawa dolna; 5 - centralna dolna; 6 - lewa dolna; 7 -
		 * lewa �rodkowa
		 */
		int left_x = x - 1;
		int right_x = x + 1;
		int top_y = y - 1;
		int bottom_y = y + 1;

		int s_ile = 0;
		
		if (left_x >= 0 && top_y >= 0)
			s[0] = this.P[left_x][top_y].prev;
		if (top_y >= 0)
			s[1] = this.P[x][top_y].prev;
		if (top_y >= 0 && right_x <= this.tabSizeX - 1)
			s[2] = this.P[right_x][top_y].prev;
		if (right_x <= this.tabSizeX - 1)
			s[3] = this.P[right_x][y].prev;
		if (bottom_y <= this.tabSizeY - 1 && right_x <= this.tabSizeX - 1)
			s[4] = this.P[right_x][bottom_y].prev;
		if (bottom_y <= this.tabSizeY - 1)
			s[5] = this.P[x][bottom_y].prev;
		if (left_x >= 0 && bottom_y <= this.tabSizeY - 1)
			s[6] = this.P[left_x][bottom_y].prev;
		if (left_x >= 0)
			s[7] = this.P[left_x][y].prev;

		// liczenie zywych sasiad�w
		s_ile = s[0] + s[1] + s[2] + s[3] + s[4] + s[5] + s[6] + s[7];
		
		if (this.P[x][y].prev == 0 && s_ile == 3)
			return 1;
		else if (this.P[x][y].prev == 1 && (s_ile == 2 || s_ile == 3))
			return 1;
		else
			return 0;
	}

	private void clickPixel(int x, int y) {
		int pixel_x = x / this.pixelSize;
		int pixel_y = y / this.pixelSize;
		int tmp_s = 0;

		if (pixel_x >= this.tabSizeX || pixel_y >= this.tabSizeY)
			return;

		tmp_s = (this.P[pixel_x][pixel_y].act == 1) ? 0 : 1;

		if (pixel_x < 1)
			pixel_x++;
		if (pixel_x >= this.tabSizeX - 1)
			pixel_x--;
		if (pixel_y < 1)
			pixel_y++;
		if (pixel_y >= this.tabSizeY - 1)
			pixel_y--;
		
			this.P[pixel_x][pixel_y].act = tmp_s;		

		this.reverse();
	}

	public void clean() {
		for (int i = 0; i < this.tabSizeX; i++) {
			for (int j = 0; j < this.tabSizeY; j++) {
				this.P[i][j].set(0);
			}
		}
	}

	private void reverse() {
		for (int i = 0; i < this.tabSizeX; i++) {
			for (int j = 0; j < this.tabSizeY; j++) {
				this.P[i][j].prev = this.P[i][j].act;
			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		for (int x = 0; x < this.tabSizeX; x++) {
			for (int y = 0; y < this.tabSizeY; y++) {

				if (this.P[x][y].act == 1) { 
					g.setColor(new Color(255, 10, 10)); 
				} else { 
					g.setColor(new Color(10, 10, 255)); 
				}

				g.fillRect(x * this.pixelSize, 
						y * this.pixelSize, 
						this.pixelSize, 
						this.pixelSize 
				);
			}
		}
	}// g

}
