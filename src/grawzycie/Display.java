package grawzycie;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Display extends JFrame {

	private static final long serialVersionUID = 1L;
	private Board panel_1 = null;
	private JPanel contentPane;
	public Display() {

		setBackground(Color.WHITE);
		setTitle("Gra w �ycie");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 648);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JButton btnPrzyspiesz = new JButton("PRZYSPIESZ");
		btnPrzyspiesz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Settings.delay -=50;
				System.out.println("Current delay:"+Settings.delay);
			}
		});
		panel.add(btnPrzyspiesz);
		
		JButton btnNewButton = new JButton("ZWOLNIJ");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Settings.delay +=50;
				System.out.println("Current delay:"+Settings.delay);
			}
		});
		panel.add(btnNewButton);

		panel_1 = new Board();
		panel_1.setBackground(SystemColor.text);
		contentPane.add(panel_1, BorderLayout.CENTER);

	    new Thread(panel_1).start();  

	    this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
            	panel_1.refresh();
            }
        });
		
		JButton btnGeneruj = new JButton("START");	
		btnGeneruj.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Settings.run = true;
			}
		});
		panel.add(btnGeneruj);
		
		JButton btnKoniec = new JButton("STOP");	
		btnKoniec.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				Settings.run = true;
			}
		});
		panel.add(btnKoniec);
		
		JButton btnReset = new JButton("CZYSC PLANSZE");
		btnReset.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Display.this.panel_1.clean();
				Display.this.panel_1.refresh();
			}
		});
		panel.add(btnReset);
		
		JLabel label = new JLabel("      ");
		panel.add(label);
	}

}